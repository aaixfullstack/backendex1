from sqlalchemy.orm import Session
from fastapi import HTTPException, status

import models
import schemas


def get_user(db: Session, user_id: int):
    return db.query(models.User).filter(models.User.id == user_id).first()


def update_user(db: Session, user_id: int, user: schemas.UserBase):
    editedUser = db.query(models.User).filter(models.User.id == user_id)
    if not editedUser.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Unable to find user id = " + user_id)
    editedUser.update(
        {"name": user.name, "email": user.email, "age": user.age})
    db.commit()
    return editedUser.first()


def delete_user(db: Session, user_id: int):
    editedUser = db.query(models.User).filter(models.User.id == user_id)
    if not editedUser.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Unable to find user id = " + user_id)
    deletedUser = editedUser.first()
    db.delete(editedUser.first())
    db.commit()
    return deletedUser


def get_user_by_email(db: Session, email: str):
    return db.query(models.User).filter(models.User.email == email).first()


def get_users(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.User).offset(skip).limit(limit).all()


def create_user(db: Session, user: schemas.UserCreate):
    fake_hashed_password = user.password
    db_user = models.User(
        email=user.email, hashed_password=fake_hashed_password, name=user.name, age=user.age)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def get_items(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.Item).offset(skip).limit(limit).all()


def create_user_item(db: Session, item: schemas.ItemCreate, user_id: int):
    db_item = models.Item(**item.dict(), owner_id=user_id)
    db.add(db_item)
    db.commit()
    db.refresh(db_item)
    return db_item
