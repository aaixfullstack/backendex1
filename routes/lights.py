from fastapi import APIRouter

router = APIRouter(prefix="/lights")

lights = {}


@router.get("/")
def get_lights():
    return [{"id": k, "status": lights[k]} for k in lights.keys()]


@router.get("/{light_id}")
def get_lights(light_id: str):
    if light_id not in lights.keys():
        lights[light_id] = 0
    return {"id": light_id, "status": lights[light_id]}


@router.post("/{light_id}")
def turn(light_id: str):
    if light_id not in lights.keys():
        lights[light_id] = 0
    else:
        lights[light_id] = 1 if lights[light_id] == 0 else 0
    return {"id": light_id, "status": lights[light_id]}
