from fastapi import APIRouter
from pydantic import BaseModel
router = APIRouter(
    prefix='/celsius'
)


class CelsiusItem(BaseModel):
    celsius: float


@router.get("/query")
async def cal_farenheit(q: str | None = None, c: float = 0):
    return {"result": c*9.0/5+32}


@router.get("/{c}")
async def cal_farenheit(c: float):
    return {"result": c*9.0/5+32}


@router.post("/")
async def cal_farenheit(item: CelsiusItem):
    return {"result": item.celsius*9.0/5+32}


@router.put("/")
async def cal_farenheit(item: CelsiusItem):
    return {"result": item.celsius*9.0/5+32}


@router.patch("/")
async def cal_farenheit(item: CelsiusItem):
    return {"result": item.celsius*9.0/5+32}
