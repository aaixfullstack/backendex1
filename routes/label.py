from typing import Annotated
from fastapi import APIRouter, File, UploadFile, status, HTTPException, Response, Form
import base64

from tempfile import NamedTemporaryFile

from io import BytesIO
import os
import cv2
router = APIRouter(
    prefix='/labelme'
)


@router.post("/", status_code=201)
async def create_upload_file(file: UploadFile,
                             text: Annotated[str, Form()] = 'BUU',
                             r: Annotated[int, Form()] = 255,
                             g: Annotated[int, Form()] = 0,
                             b: Annotated[int, Form()] = 0,
                             a: Annotated[int, Form()] = 0,
                             fontScale: Annotated[int, Form()] = 1):
    temp = NamedTemporaryFile(delete=False)
    try:
        try:
            contents = file.file.read()
            with temp as f:
                f.write(contents)
            temp.close()
        except Exception as ex:
            raise HTTPException(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=ex.__str__())

        finally:
            file.file.close()
        return process(temp.name, text, (b, g, r, a), fontScale)
    except Exception as ex:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=ex.__str__())
    finally:
        # os.remove(temp.name)
        pass


def process(path, text, color=(255, 0, 0, 1), fontScale=1):
    image = cv2.imread(path)

    # font
    font = cv2.FONT_HERSHEY_SIMPLEX

    # org
    org = (50, 50)

    # # Blue color in BGR
    # color = (255, 0, 0)

    # Line thickness of 2 px
    thickness = 2

    # Using cv2.putText() method
    img = cv2.putText(image, text, org, font,
                      fontScale, color, thickness, cv2.LINE_AA)
    is_success, im_png = cv2.imencode(".png", img)
    encode_string = base64.b64encode(im_png.tobytes()).decode('utf-8')
    return {"result": 'data:image/png;base64,' + encode_string}
